﻿using Grpc.Net.Client;
using Server.Protos;
using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new ChooseSeason.ChooseSeasonClient(channel);
            string date = Console.ReadLine();
            if (Check(date))
                try
                {
                    DateTime toDate = Convert.ToDateTime(date);
                    var input = new Request
                    {
                        Day = toDate.Day,
                        Month = toDate.Month,
                        Year = toDate.Year
                    };
                    Console.WriteLine(client.getSign(input).Sign);
                }
                catch
                {
                    Console.WriteLine("Invalid date!");
                }
            else Console.WriteLine("Invalid date format!");
            Console.ReadLine();
        }

        private static bool Check(string date)
        {
            foreach (char c in date)
                if ((c < '0' && c > '9') && c != '/')
                    return false;
            return true;
        }
    }
}

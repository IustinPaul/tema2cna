﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server
{
    public class Sign
    {
        private string name;
        private DateTime signStart;
        private DateTime signEnd;

        public Sign(string name, string signStart, string signEnd)
        {
            this.name = name;
            this.signStart = Convert.ToDateTime(signStart);
            this.signEnd = Convert.ToDateTime(signEnd);
        }

        public string getName()
        {
            return name;
        }

        public DateTime getSignStart()
        {
            return signStart;
        }
        public DateTime getSignEnd()
        {
            return signEnd;
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Server
{
    public class Program
    {
        public static List<Sign> SignSpring = new List<Sign>();
        public static List<Sign> SignSummer = new List<Sign>();
        public static List<Sign> SignAutumn = new List<Sign>();
        public static List<Sign> SignWinter = new List<Sign>();
        
        public static void Main(string[] args)
        {
            AddSigns();
            CreateHostBuilder(args).Build().Run();
        }

        // Additional configuration is required to successfully run gRPC on macOS.
        // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static void AddSigns()
        {
            System.IO.StreamReader file = new System.IO.StreamReader("Signs.txt");
            string name;
            while((name = file.ReadLine())!=null)
            {
                string startDate = file.ReadLine();
                string endDate = file.ReadLine();
                Sign sign = new Sign(name, startDate, endDate);

                if ((sign.getSignStart().Month >= 3 && sign.getSignStart().Month <= 5) ||
                    (sign.getSignEnd().Month >= 3 && sign.getSignEnd().Month <= 5))
                    SignSpring.Add(sign);

                if ((sign.getSignStart().Month >= 6 && sign.getSignStart().Month <= 8) ||
                    (sign.getSignEnd().Month >= 6 && sign.getSignEnd().Month <= 8))
                    SignSummer.Add(sign);

                if ((sign.getSignStart().Month >= 9 && sign.getSignStart().Month <= 11) ||
                    (sign.getSignEnd().Month >= 9 && sign.getSignEnd().Month <= 11))
                    SignAutumn.Add(sign);

                if ((sign.getSignStart().Month == 12 || sign.getSignStart().Month <= 2) ||
                    (sign.getSignEnd().Month == 12 || sign.getSignEnd().Month <= 2))
                    SignWinter.Add(sign);
            }
        }
    }
}

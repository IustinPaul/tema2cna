﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class AutumnService : Autumn.AutumnBase
    {
        private object _logger;

        public AutumnService(ILogger<AutumnService> logger)
        {
            _logger = logger;
        }

        public override Task<AutumnReply> GetAutumnSign(AutumnRequest request, ServerCallContext context)
        {
            string signName=String.Empty;
            foreach(Sign sign in Program.SignAutumn)
            {
                if (request.Month == sign.getSignStart().Month)
                {
                    if (request.Day >= sign.getSignStart().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
                else if (request.Month == sign.getSignEnd().Month)
                {
                    if (request.Day <= sign.getSignEnd().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
            }

            AutumnReply output = new AutumnReply()
            {
                Sign = signName
            };

            return Task.FromResult(output);
        }
    }
}

﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class SpringService : Spring.SpringBase
    {
        private object _logger;

        public SpringService(ILogger<SpringService> logger)
        {
            _logger = logger;
        }
        public override Task<SpringReply> GetSpringSign(SpringRequest request, ServerCallContext context)
        {
            string signName = String.Empty;
            foreach (Sign sign in Program.SignSpring)
            {
                if (request.Month == sign.getSignStart().Month)
                {
                    if (request.Day >= sign.getSignStart().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
                else if (request.Month == sign.getSignEnd().Month)
                {
                    if (request.Day <= sign.getSignEnd().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
            }

            SpringReply output = new SpringReply()
            {
                Sign = signName
            };

            return Task.FromResult(output);
        }
    }
}

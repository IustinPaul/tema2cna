﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class SummerService : Summer.SummerBase
    {
        private object _logger;

        public SummerService(ILogger<SummerService> logger)
        {
            _logger = logger;
        }

        public override Task<SummerReply> GetSummerSign(SummerRequest request, ServerCallContext context)
        {
            string signName = String.Empty;
            foreach (Sign sign in Program.SignSummer)
            {
                if (request.Month == sign.getSignStart().Month)
                {
                    if (request.Day >= sign.getSignStart().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
                else if (request.Month == sign.getSignEnd().Month)
                {
                    if (request.Day <= sign.getSignEnd().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
            }

            SummerReply output = new SummerReply()
            {
                Sign = signName
            };

            return Task.FromResult(output);
        }
    }
}

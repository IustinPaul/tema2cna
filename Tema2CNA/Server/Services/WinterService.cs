﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class WinterService : Winter.WinterBase
    {
        private object _logger;

        public WinterService(ILogger<WinterService> logger)
        {
            _logger = logger;
        }

        public override Task<WinterReply> GetWinterSign(WinterRequest request, ServerCallContext context)
        {
            string signName = String.Empty;
            foreach (Sign sign in Program.SignWinter)
            {
                if (request.Month == sign.getSignStart().Month)
                {
                    if (request.Day >= sign.getSignStart().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
                else if(request.Month == sign.getSignEnd().Month)
                {
                    if(request.Day <= sign.getSignEnd().Day)
                    {
                        signName = sign.getName();
                        break;
                    }
                }
            }

            WinterReply output = new WinterReply()
            {
                Sign = signName
            };

            return Task.FromResult(output);
        }
    }
}

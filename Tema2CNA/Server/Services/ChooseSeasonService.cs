﻿using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class ChooseSeasonService : ChooseSeason.ChooseSeasonBase
    {
        private object _logger;

        public ChooseSeasonService(ILogger<ChooseSeasonService> logger)
        {
            _logger = logger;
        }

        public override Task<Reply> getSign(Request request, ServerCallContext context)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            Reply output = new Reply();
            if (request.Month>=3 && request.Month<=5)
            {
                var season = new Spring.SpringClient(channel);
                var input = new SpringRequest {
                    Day = request.Day,
                    Month = request.Month,
                    Year = request.Year
                };
                output.Sign = season.GetSpringSign(input).Sign;
            }
            else if (request.Month >= 6 && request.Month <= 8)
            {
                var season = new Summer.SummerClient(channel);
                var input = new SummerRequest
                {
                    Day = request.Day,
                    Month = request.Month,
                    Year = request.Year
                };
                output.Sign = season.GetSummerSign(input).Sign;
            }
            else if (request.Month >= 9 && request.Month <= 11)
            {
                var season = new Autumn.AutumnClient(channel);
                var input = new AutumnRequest
                {
                    Day = request.Day,
                    Month = request.Month,
                    Year = request.Year
                };
                output.Sign = season.GetAutumnSign(input).Sign;
            }
            else
            {
                var season = new Winter.WinterClient(channel);
                var input = new WinterRequest
                {
                    Day = request.Day,
                    Month = request.Month,
                    Year = request.Year
                };
                output.Sign = season.GetWinterSign(input).Sign;
            }
            return Task.FromResult(output);
        }
    }
}
